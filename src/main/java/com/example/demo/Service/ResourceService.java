package com.example.demo.Service;

import com.example.demo.Model.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ResourceService {
    public Resource createResource(String name);

    public Resource editResource(String id, String name) throws Exception;

    public List<Resource> findAll(String filter);

    public Resource findById(String id) throws Exception;

    public void delete(String id);
}
