package com.example.demo.Service;

import com.example.demo.Model.Resource;
import org.springframework.data.repository.CrudRepository;

public interface ResourceServiceRepository extends CrudRepository<Resource, String> {
}
