package com.example.demo.Service;

import com.example.demo.Model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceServiceRepository repository;

    @Override
    public Resource createResource(String name) {
        Resource resource = new Resource();
        resource.setName(name);
        resource.setId(UUID.randomUUID().toString());
        return repository.save(resource);
    }

    @Override
    public Resource editResource(String id, String name) throws Exception {
        Resource resource = getResource(id);
        resource.setName(name);
        return repository.save(resource);
    }

    @Override
    public List<Resource> findAll(String filter) {
        List<Resource> resources = new ArrayList<>();
        repository.findAll().forEach(resources::add);
        return resources.stream()
                .filter(resource -> resource.name.toLowerCase().contains(filter.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public Resource findById(String id) throws Exception {
        return getResource(id);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }

    private Resource getResource(String id) throws Exception {
        Optional<Resource> resourceOptional = repository.findById(id);
        if (!resourceOptional.isPresent()) {
            throw new Exception("Resource not found");
        }

        return resourceOptional.get();
    }
}
