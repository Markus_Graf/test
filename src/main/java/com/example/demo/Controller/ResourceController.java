package com.example.demo.Controller;

import com.example.demo.Model.Resource;
import com.example.demo.Service.ResourceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/Resource/")
public class ResourceController {

    private final ResourceService resourceService;

    ResourceController(ResourceService resourceService) {
        this.resourceService = resourceService;
    }

    @PostMapping("/create")
    public ResponseEntity<Resource> create(@RequestParam String name) {
        Resource resource = resourceService.createResource(name);
        return new ResponseEntity(resource, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Resource> update(@RequestParam String id, @RequestParam String name) {
        try {
            Resource resource = resourceService.editResource(id, name);
            return new ResponseEntity(resource, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<Resource>> findAll(@RequestParam String filter) {
        List<Resource> resources = resourceService.findAll(filter);
        return new ResponseEntity(resources, HttpStatus.OK);
    }

    @GetMapping("/findById")
    public ResponseEntity<Resource> findById(@RequestParam String id) {
        try {
            Resource resource = resourceService.findById(id);
            return new ResponseEntity(resource, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<HttpStatus> delete(@RequestParam String id) {
        resourceService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
